library csusb;

import 'package:hex/hex.dart';
import 'package:process_run/shell.dart';

/// Csusb wrapper
class Csusb {
  String path;

  Csusb(this.path);

  CsusbDevice device(int vid, int pid) {
    return CsusbDevice(this, vid, pid);
  }
}

/// Csusb device
class CsusbDevice {
  Csusb csusb;
  int pid;
  int vid;

  CsusbDevice(this.csusb, this.vid, this.pid);

  String _hexPid() {
    return '0x${pid.toRadixString(16).padLeft(4, '0')}';
  }

  String _hexVid() {
    return '0x${vid.toRadixString(16).padLeft(4, '0')}';
  }

  /// Write data use csusb
  Future<WriteResult> _write(DataValueMode mode, String value) async {
    String modeText;
    switch (mode) {
      case DataValueMode.file:
        modeText = '--file';
        break;
      case DataValueMode.hex:
        modeText = '--hex';
        break;
      case DataValueMode.raw:
        modeText = '--raw';
        break;
      case DataValueMode.binary:
        modeText = '--binary --file';
        break;
    }
    String command =
        '${csusb.path} --vid ${_hexVid()} --pid ${_hexPid()} $modeText --value $value';
    var shell = Shell();
    try {
      var result = await shell.run(command);
      int exitCode = 0;
      String output = '';
      for (var item in result) {
        if (item.exitCode != 0) {
          exitCode = item.exitCode;
        }
        var line = item.exitCode == 0 ? item.stdout : item.stderr;
        output += line + '\n';
      }
      return WriteResult(exitCode, command, output);
    } on ShellException catch (e) {
      var message = e.message;
      var result = e.result;
      var exitCode = result?.exitCode ?? 1;
      String? output = exitCode == 0 ? result?.stdout : result?.stderr;
      return WriteResult(exitCode, command, output ?? '',
          errorMessage: message);
    }
  }

  /// Write with file
  Future<WriteResult> writeWithFile(String filePath) async {
    return await _write(DataValueMode.file, filePath);
  }

  /// Write with hex
  Future<WriteResult> writeWithHex(List<int> bytes) async {
    var hex = HEX.encode(bytes);
    return await _write(DataValueMode.hex, hex);
  }

  /// Write with raw string content
  Future<WriteResult> writeWithRaw(String raw) async {
    return await _write(DataValueMode.raw, raw);
  }

  /// Write with binary, the arguments is file path
  Future<WriteResult> writeWithBinary(String filePath) async {
    return await _write(DataValueMode.binary, filePath);
  }
}

/// Print mode
enum DataValueMode {
  file,
  hex,
  raw,
  binary,
}

/// Print resullt
class WriteResult {
  int exitCode;
  String command;
  String output;
  String? errorMessage;

  WriteResult(this.exitCode, this.command, this.output, {this.errorMessage});
}
