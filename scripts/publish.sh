#!/bin/sh
#

BIN_PATH=$(dirname $(readlink -f $0))
WORK_PATH=${BIN_PATH}/../


# -p:TargetFramework=netcoreapp3.1 \
# -p:TargetFramework=net5.0 \

dotnet publish \
  -c Release \
  -r win-x86 \
  -p:TargetFramework=net5.0 \
  -p:PublishSingleFile=true \
  -p:PublishTrimmed=true \
  -p:PublishReadyToRun=true \
  -p:IncludeNativeLibrariesForSelfExtract=true \
  -p:PublishReadyToRunShowWarnings=true \
  --self-contained true \
  ${WORK_PATH}/csusb.sln
