
cd /d %~dp0

cd ../


rem dotnet publish -c Release -r win-x86 -p:PublishSingleFile=true --self-contained true

rem -p:TargetFramework=netcoreapp3.1 \
rem -p:TargetFramework=net5.0 \

dotnet publish ^
  -c Release ^
  -r win-x86 ^
  -p:TargetFramework=net5.0 ^
  -p:PublishSingleFile=true ^
  -p:PublishTrimmed=true ^
  -p:PublishReadyToRun=true ^
  -p:IncludeNativeLibrariesForSelfExtract=true ^
  -p:PublishReadyToRunShowWarnings=true ^
  --self-contained true ^
  ./csusb.sln

