﻿using CommandLine;
using System;
using System.Collections.Generic;

namespace csusb
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Options>(args)
               .WithParsed(RunOptions)
               .WithNotParsed(HandleParseError);
        }
        static void RunOptions(Options opts)
        {
            new Handler().Handle(opts);
        }
        static void HandleParseError(IEnumerable<Error> errs)
        {
            //handle errors
            Console.WriteLine("Missing parameters");
        }
    }
}
