﻿using System;
using System.Runtime.InteropServices;

namespace csusb
{
    class USBIO
    {

        [DllImport("USBIO", EntryPoint = "USBIO_SetDeviceID")]
        public static extern void USBIO_SetDeviceID(Guid newGUID);

        [DllImport("USBIO", EntryPoint = "USBIO_OpenDevice")]
        public static extern IntPtr USBIO_OpenDevice(uint dwDeviceNum, string vidpid);  // Input <Future Use>

        [DllImport("USBIO", EntryPoint = "USBIO_IsConnected")]
        public static extern bool USBIO_IsConnected();

        [DllImport("USBIO", EntryPoint = "USBIO_CloseDevice")]
        public static extern bool USBIO_CloseDevice(IntPtr handle);
        [DllImport("USBIO", EntryPoint = "USBIO_GetDeviceCount")]
        public static extern uint USBIO_GetDeviceCount();


        [DllImport("USBIO", EntryPoint = "USBIO_Read")]
        public static extern uint USBIO_Read(byte[] pData,            // Output
                uint dwLen,            // Input
                ref uint pLength,         // Output
                uint dwMilliseconds);   // Input
        [DllImport("USBIO", EntryPoint = "USBIO_Write")]
        public static extern uint USBIO_Write(byte[] pData,           // Input
                 uint dwLen,           // Input
                 ref uint pLength,        // Output
                 uint dwMilliseconds);  // Input
        [DllImport("USBIO", EntryPoint = "USBIO_ReadEp")]
        public static extern uint USBIO_ReadEp(out System.UInt32 pData,         // Output
                   uint dwLen,         // Input
                   out System.Int32[] pLength,      // Output
                   uint dwMilliseconds);// Input
        [DllImport("USBIO", EntryPoint = "USBIO_WriteEp")]
        public static extern uint USBIO_WriteEp(ref byte[] pData,          // Input
                   uint dwLen,         // Input
                   out System.Int32[] pLength,      // Output
                   uint dwMilliseconds);// Input


        [DllImport("USBIO", EntryPoint = "USBIO_GetDevicePathName")]

        public static extern string USBIO_GetDevicePathName();

        public IntPtr mHanlder;
        public uint dataout = 0;




        public bool OpenDevide(Guid newGUID, string VidPid)
        {
            USBIO_SetDeviceID(newGUID);
            for (uint i = 0; i < 127; i++)
            {

                mHanlder = USBIO_OpenDevice(i, VidPid);
                if (mHanlder != (IntPtr)(-1))
                {
                    return true;

                }
            }
            return false;
        }

        public bool CloseDevice()
        {
            if (mHanlder == null)
            {
                return true;
            }
            return USBIO_CloseDevice(mHanlder);
        }

        public void SendData(byte[] data, uint delay)
        {
            if (mHanlder != (IntPtr)(-1))
            {
                USBIO_Write(data, (uint)data.Length, ref dataout, delay);
            }
        }

        public byte[] ReadData(uint delay)
        {
            byte[] rev = new byte[64];

            if (mHanlder != (IntPtr)(-1))
            {
                USBIO_Read(rev, (uint)rev.Length, ref dataout, delay);
            }

            byte[] rev2 = new byte[dataout];

            for (int i = 0; i < dataout; i++)
            {
                rev2[i] = rev[i];
            }

            return rev2;
        }

    }
}
