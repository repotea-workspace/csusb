﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace csusb
{
    class Handler
    {
        private static readonly string strid = "28d78fad-5a12-11d1-ae5b-0000f803a8c2";
        private static readonly Guid usbguid = new Guid(strid);

        string ParseDeviceId(string id)
        {
            id = id.ToLower();
            if (id.Contains("0x"))
            {
                id = id.Replace("0x", "");
                return id;
            }
            int _id = int.Parse(id);
            return _id.ToString("X");
            // return id;
        }

        public void Handle(Options opts)
        {
            String result = Check(opts);
            if (result != null)
            {
                TextWriter errorWriter = Console.Error;
                errorWriter.WriteLine("Arguments check failed: " + result);
                Environment.Exit(1);
                return;
            }
            USBIO usbio = new USBIO();
            try
            {
                string vid = ParseDeviceId(opts.vid);
                string pid = ParseDeviceId(opts.pid);
                if (opts.verbose)
                {
                    Console.WriteLine("vid: " + opts.vid + " -> " + vid);
                    Console.WriteLine("pid: " + opts.pid + " -> " + pid);
                }
                string vidpid = "vid_" + vid + "&pid_" + pid;
                bool opened = usbio.OpenDevide(usbguid, vidpid);
                if (!opened)
                {
                    TextWriter errorWriter = Console.Error;
                    errorWriter.WriteLine("Failed to open USB");
                    Environment.Exit(1);
                    return;
                }
                if (opts.verbose)
                {
                    Console.WriteLine("Successed to connect USB");
                }

                if (opts.binary && opts.file)
                {
                    byte[] binary = File.ReadAllBytes(opts.value);
                    HandleBinary(usbio, opts, binary);
                    return;
                }
                string value;
                if (opts.file)
                {
                    if (opts.verbose)
                    {
                        Console.WriteLine("File path: " + opts.value);
                    }
                    value = File.ReadAllText(opts.value);
                } else
                {
                    value = opts.value;
                }
                if (opts.hex)
                {
                    HandleHex(usbio, opts, value);
                }
                if (opts.raw)
                {
                    HandleRaw(usbio, opts, value);
                }
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            } finally
            {
                usbio.CloseDevice();
            }

        }

        String Check(Options opts)
        {
            if (!opts.binary && !opts.raw && !opts.hex)
            {
                return "The value type is required, please set --raw or --hex or --binary";
            }
            if (opts.binary && !opts.file)
            {
                return "If the value type is binary, you must be set --file to parameters and --value must be a file path";
            }
            if (opts.hex && opts.raw)
            {
                return "You can only choose --hex or --raw";
            }
            if (opts.file)
            {
                if (!File.Exists(opts.value))
                {
                    return "The value is not a file";
                }
                FileAttributes attr = File.GetAttributes(opts.value);
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    return "The value is not a file, it's a directory";
                }
            }
            return null;
        }


        void HandleBinary(USBIO usbio, Options opts, byte[] binary) {
            if (opts.verbose)
            {
                string output = ToHexBest(binary);
                Console.WriteLine("======");
                Console.WriteLine(output);
                Console.WriteLine("======");
            }
            usbio.SendData(binary, 100);
        }

        void HandleRaw(USBIO usbio, Options opts, String value)
        {
            if (opts.verbose)
            {
                Console.WriteLine("Command:");
                Console.WriteLine("======");
                Console.WriteLine(value);
                Console.WriteLine("======");
            }
            value = value.Replace("\r", "");
            string[] arrs = value.Split("\n");
            List<byte> outputs = new List<byte>();
            for (int i=0; i<arrs.Length; i++)
            {
                string line = arrs[i];
                if (line == null || line.Length == 0)
                {
                    continue;
                }
                line += "\r\n";
                byte[] bytes = Encoding.Default.GetBytes(line);
                for (int j=0; j< bytes.Length; j++)
                {
                    outputs.Add(bytes[j]);
                }
            }
            byte[] binary = outputs.ToArray();

            // byte[] binary = Encoding.Default.GetBytes(value);
            HandleBinary(usbio, opts, binary);
        }

        void HandleHex(USBIO usbio, Options opts, String value)
        {
            if (opts.verbose)
            {
                Console.WriteLine("======");
                Console.WriteLine(value);
                Console.WriteLine("======");
            }
            value = value.ToLower();
            if (value.Contains("0x"))
            {
                value = value.Replace("0x", "");
            }
            byte[] binary = this.StringToByteArray(value);
            HandleBinary(usbio, opts, binary);
        }

        byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }



        String ToHexBest(byte[] binary)
        {
            string output = "";
            string hex = ToHex(binary);
            for (int i=0; i<hex.Length; i ++)
            {
                int ri = i + 1;
                char ch = hex[i];
                output += ch.ToString();
                if (ri %2 == 0)
                {
                    output += ' ';
                }
                if (ri % 32 == 0)
                {
                    output += "\r\n";
                }
            }
            return output;
        }

        string ToHex(byte[] bytes)
        {
            char[] c = new char[bytes.Length * 2];

            byte b;

            for (int bx = 0, cx = 0; bx < bytes.Length; ++bx, ++cx)
            {
                b = ((byte)(bytes[bx] >> 4));
                c[cx] = (char)(b > 9 ? b - 10 + 'A' : b + '0');

                b = ((byte)(bytes[bx] & 0x0F));
                c[++cx] = (char)(b > 9 ? b - 10 + 'A' : b + '0');
            }

            return new string(c);
        }

    }
}
