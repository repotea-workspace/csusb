﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace csusb
{
	enum DataFormat
    {
		Hex,
		Raw,
    }

	class Options
	{
		// Omitting long name, defaults to name of property, ie "--verbose"
		[Option(
			'v', "verbose",
		  Default = false,
		  HelpText = "Prints all messages to standard output.")]
		public bool verbose { get; set; }

		// USB Vendor ID
		[Option("vid", Required = true, HelpText = "Vendor ID")]
		public string vid { get; set; }

		// USB Product ID
		[Option("pid", Required = true, HelpText = "Product ID")]
		public string pid { get; set; }

		// THe value is a file path
		[Option("file", Required = false, HelpText = "The value is a file path")]
		public bool file { get; set; }

		// The value data is hex string
		[Option("hex", Required = false, HelpText = "The value data is hex string")]
		public bool hex { get; set; }

		// The value data is raw string
		[Option("raw", Required = false, HelpText = "The value data is raw string")]
		public bool raw { get; set; }

		// The value data is binary
		[Option("binary", Required = false, HelpText = "The value data is binary, If this you should be use --file and value is a file path")]
		public bool binary { get; set; }

		// The value
		[Option("value", Required = true, HelpText = "Value")]
		public string value { get; set; }
	}
}
